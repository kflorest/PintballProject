# pintball

> Pinball The grid Theme

## Build Setup

``` bash
# install dependencies
npm install

# run all tests
npm test


# Informaçoões Adicionais
- Eu tinha uma duvida sobre o design do layout, eu resolvi fazer o site do jeito mais parecido com o template mandado em photoshop, mas a minha duvida era - se eu tinha que fazer um desenho novo.
- Fiz dsploy do projeto em Heroku.
- O Search procura por Categoria: ('3D Animations', 'Animations Films').
- Utilizei Vue como framework Javascript para o desenvolvimento.
- Utilizei Vuex.
- Utilizei Burma como Framework de CSS.
- O Site é responsive.
- As informações do site carregadas nos Cards são fixas, não são fornecidas por uma api.


For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
