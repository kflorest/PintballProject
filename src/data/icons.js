const location = require('../assets/ic_location_on_black_24px.svg')
const star = require('../assets/ic_star_black_24px.svg')
const starBordered = require('../assets/ic_star_border_black_24px.svg')
const starHalf = require('../assets/ic_star_half_black_24px.svg')
const search = require('../assets/ic_search_black_24px.svg')
const facebook = require('../assets/ic_facebook_black_24px.svg')

const icons = {
  category: location,
  star: star,
  starBordered: starBordered,
  starHalf: starHalf,
  search: search,
  facebook: facebook
}

export default icons
