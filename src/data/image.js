const movieImages = [
  require('../assets/image-001.jpg'),
  require('../assets/image-002.jpg'),
  require('../assets/image-003.jpg'),
  require('../assets/image-004.jpg')
]

export default movieImages
