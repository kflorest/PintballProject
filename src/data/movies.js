import movieImages from './image'

const movies = [
  {
    image: {
      src: movieImages[0],
      alt: 'Image'
    },
    title: 'Animation Films',
    category: 'Movies',
    description: 'Pariatur dolore consectetur ex officia dolor quis ipsum ullamco exercitation commodo.',
    score: 2
  },
  {
    image: {
      src: movieImages[1],
      alt: 'Image'
    },
    title: 'Animation Films',
    category: '3D Animations',
    description: 'Incididunt amet non cupidatat voluptate dolor ut adipisicing.',
    score: 3
  },
  {
    image: {
      src: movieImages[2],
      alt: 'Image'
    },
    title: 'Animation Films',
    category: '3D Animations',
    description: 'Ullamco deserunt do eiusmod anim in irure ea enim. Nostrud amet reprehenderit occaecat ex tempor ullamco commodo nulla labore tempor consequat quis nostrud reprehenderit.',
    score: 3
  },
  {
    image: {
      src: movieImages[3],
      alt: 'Image'
    },
    title: 'Animation Films',
    category: 'Movies',
    description: 'Officia pariatur enim deserunt nisi laboris ex velit dolore esse.',
    score: 3
  },
  {
    image: {
      src: movieImages[0],
      alt: 'Image'
    },
    title: 'Animation Films',
    category: 'Movies',
    description: 'Pariatur dolore consectetur ex officia dolor quis ipsum ullamco exercitation commodo.',
    score: 2
  },
  {
    image: {
      src: movieImages[3],
      alt: 'Image'
    },
    title: 'Animation Films',
    category: 'Movies',
    description: 'Officia pariatur enim deserunt nisi laboris ex velit dolore esse.',
    score: 3
  },
  {
    image: {
      src: movieImages[1],
      alt: 'Image'
    },
    title: 'Animation Films',
    category: '3D Animations',
    description: 'Officia ea elit ullamco qui. Culpa officia duis consequat ut in elit pariatur laborum eu cupidatat consectetur amet.',
    score: 3
  },
  {
    image: {
      src: movieImages[2],
      alt: 'Image'
    },
    title: 'Animation Films',
    category: '3D Animations',
    description: 'Quis excepteur duis tempor ut magna eu dolore qui deserunt elit veniam labore.',
    score: 3
  },
  {
    image: {
      src: movieImages[3],
      alt: 'Image'
    },
    title: 'Animation Films',
    category: 'Movies',
    description: 'Officia pariatur enim deserunt nisi laboris ex velit dolore esse.',
    score: 3
  },
  {
    image: {
      src: movieImages[1],
      alt: 'Image'
    },
    title: 'Animation Films',
    category: '3D Animations',
    description: 'Incididunt amet non cupidatat voluptate dolor ut adipisicing.',
    score: 3
  },
  {
    image: {
      src: movieImages[0],
      alt: 'Image'
    },
    title: 'Animation Films',
    category: 'Movies',
    description: 'Pariatur dolore consectetur ex officia dolor quis ipsum ullamco exercitation commodo.',
    score: 2
  }
]

export default movies
