/**
 * Search Module
 */

const state = {
  search: ''
}
// Actions
const actions = {
  SEARCH ({ commit }, obj) {
    commit('SEARCH', { search: obj })
  }
}
// Mutations
const mutations = {
  SEARCH: (scope, { search }) => {
    const s = scope
    s.search = search
  }
}
// Getters
const getters = {
  getSearch: scope => scope.search
}

const module = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}

export default module
