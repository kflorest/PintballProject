import Vue from 'vue'
import Vuex from 'vuex'

// Import Modules
import ModuleSearch from './modules/search'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    ModuleSearch
  }
})
